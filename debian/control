Source: squirrel3
Maintainer: Fabian Wolff <fabi.wolff@arcor.de>
Section: interpreters
Priority: optional
Build-Depends: debhelper-compat (= 12),
               cmake,
               python3-sphinx | python-sphinx,
               texlive,
               texlive-latex-extra,
               texlive-plain-generic,
               latexmk
Standards-Version: 4.4.0
Homepage: http://squirrel-lang.org/
Vcs-Git: https://salsa.debian.org/wolff-guest/squirrel3.git/
Vcs-Browser: https://salsa.debian.org/wolff-guest/squirrel3

Package: squirrel3
Architecture: any
Multi-Arch: no
Depends: libsquirrel3-0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: lightweight, high-level, embeddable programming language
 Squirrel is a high-level imperative, object-oriented programming
 language, designed to be a lightweight scripting language that fits
 in the size, memory bandwidth, and real-time requirements of
 applications like video games.
 .
 This package contains the Squirrel command line interpreter and
 bytecode compiler.

Package: libsquirrel3-0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: runtime libraries for the Squirrel programming language
 Squirrel is a high-level imperative, object-oriented programming
 language, designed to be a lightweight scripting language that fits
 in the size, memory bandwidth, and real-time requirements of
 applications like video games.
 .
 This package contains runtime libraries. You shouldn't have to
 install it manually.

Package: libsquirrel-dev
Architecture: any
Multi-Arch: no
Section: libdevel
Depends: libsquirrel3-0 (= ${binary:Version}),
         ${misc:Depends}
Description: development files for the Squirrel programming language
 Squirrel is a high-level imperative, object-oriented programming
 language, designed to be a lightweight scripting language that fits
 in the size, memory bandwidth, and real-time requirements of
 applications like video games.
 .
 This package contains developer resources for using the Squirrel
 library. Install it if you are developing programs that use the
 Squirrel API.

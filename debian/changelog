squirrel3 (3.1-8) unstable; urgency=medium

  * Change build dependency from texlive-generic-extra to
    texlive-plain-generic (Closes: #933589).
  * Upgrade to debhelper compat level 12 (no changes).
  * Upgrade to Standards-Version 4.4.0 (no changes).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Thu, 01 Aug 2019 15:01:06 +0200

squirrel3 (3.1-7) unstable; urgency=medium

  * Revert upgrade to debhelper compat level 12.
  * Update meta-information of 02-sphinx-ext.patch.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Wed, 20 Mar 2019 20:37:02 +0100

squirrel3 (3.1-6) unstable; urgency=medium

  * Update Vcs-Git and Vcs-Browser fields in debian/control.
  * Add patch 02-sphinx-ext.patch to disable the pngmath Sphinx
    extension (Closes: #923012).
  * Add Applied-Upstream field to header of patch
    01-fix-spelling-errors.patch.
  * Upgrade to Standards-Version 4.3.0 (no changes).
  * Upgrade to debhelper compat level 12.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 03 Mar 2019 01:37:29 +0100

squirrel3 (3.1-5) unstable; urgency=medium

  * Update debian/copyright.
  * Set Priority to "optional" in debian/control.
  * Upgrade to Standards-Version 4.1.0 in debian/control.
  * Upgrade to compat level 10.
  * Adjust Multi-Arch information in debian/control.
  * Add build-dependency on latexmk (Closes: #872264).
  * No longer modify generated PDFs in debian/rules (pdflatex should
    automatically build them reproducibly by now).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 17 Sep 2017 21:31:30 +0200

squirrel3 (3.1-4) unstable; urgency=medium

  * Delete libsquirrel3-0.symbols.amd64 (Closes: #831210).
  * Modify Lintian override to apply to all architectures.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Fri, 22 Jul 2016 13:43:59 +0200

squirrel3 (3.1-3) unstable; urgency=medium

  * Add Vcs-Git and Vcs-Browser fields to debian/control.
  * Fix FTBFS with new sphinx version by adding texlive-generic-extra
    to build dependencies in debian/control (Closes: #828963).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Wed, 29 Jun 2016 16:08:58 +0200

squirrel3 (3.1-2) unstable; urgency=medium

  * Upgrade to Standards version 3.9.8 (no changes).
  * Start short description lines with minuscules in debian/control
    (Closes: #821029).
  * Override Lintian false positive in libsquirrel3-0.
  * Make sure sphinx-build never performs network connections.
  * Improve reproducibility of PDF documents.
  * Set "Multi-Arch: foreign" for -dev package (Closes: #820529).
  * Add configuration file for git-buildpackage (debian/gbp.conf).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 24 Apr 2016 21:07:47 +0200

squirrel3 (3.1-1) unstable; urgency=low

  * Initial release. Closes: #651195

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 03 Apr 2016 20:14:18 +0200

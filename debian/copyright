Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: squirrel
Upstream-Contact: Alberto Demichelis <alberto@demichelis.net>
Source: https://www.github.com/albertodemichelis/squirrel/

Files:	   *
Copyright: 2003-2016 Alberto Demichelis
License:   Expat

Files:     debian/*
Copyright: 2016-2017 Fabian Wolff <fabi.wolff@arcor.de>
License:   Expat

Files:     squirrel/sqtable.h
           squirrel/sqstate.h
Copyright: 2003-2016 Alberto Demichelis
           1994-2002 TeCGraf, PUC-Rio <lua@tecgraf.puc-rio.br>
Comment:   Parts of the source code were originally based on Lua 4.0 code.
           http://www.lua.org/source/4.0.1/src_ltable.c.html
           http://www.lua.org/source/4.0.1/src_lstring.c.html
License:   Expat and Lua

License:   Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License:   Lua
 Permission is hereby granted, without written agreement and without license
 or royalty fees, to use, copy, modify, and distribute this software and its
 documentation for any purpose, including commercial applications, subject to
 the following conditions:
 .
 * The above copyright notice and this permission notice shall appear in all
   copies or substantial portions of this software.
 .
 * The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software in a
   product, an acknowledgment in the product documentation would be greatly
   appreciated (but it is not required).
 .
 * Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
 .
 The authors specifically disclaim any warranties, including, but not limited
 to, the implied warranties of merchantability and fitness for a particular
 purpose. The software provided hereunder is on an "as is" basis, and the
 authors have no obligation to provide maintenance, support, updates,
 enhancements, or modifications. In no event shall TeCGraf, PUC-Rio, or the
 authors be held liable to any party for direct, indirect, special,
 incidental, or consequential damages arising out of the use of this software
 and its documentation.
